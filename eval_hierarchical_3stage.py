##########
# Template for executing model training / inference 
##########

##### Imports
## 3rd party lib
import argparse
from datetime import datetime
import gc
#import matplotlib.pyplot as plt
import numpy as np
import os
import random
import torch
import torchvision.datasets as datasets
import yaml

## Custom lib
from src.networks.networks import *
from src.util.common import set_seeds, get_threshold_mask
from src.util.load_dataset import *


##### Constants
## Arguments
parser = argparse.ArgumentParser(description='NN Template - Experiment')
#parser.add_argument('id', type=str, help='a unique id for experiment identification')
parser.add_argument('--config-path', action='store_true', default='config/config_hierarchical.yml',
                    help='path to config file of parameters')
ARGS = parser.parse_args()

## Paths
DATA_PATH = "data"
PRETRAIN_PATH = "pretrained_models"
#RESULTS_PATH = "results/" + ARGS.id + "/" + datetime.now().strftime("%Y-%m-%d_%H%M%S")
#if not os.path.exists(RESULTS_PATH): os.makedirs(RESULTS_PATH)

##### Source begin

### Experiment Setup (begin) ###
## parse YAML file for input parameter
print("Parse YAML...")
with open(ARGS.config_path, 'r') as stream:
    try:
        params = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)

## Setup CUDA (or not)
print("Setup CUDA...")
cuda = not params["no_cuda"] and torch.cuda.is_available()
if cuda:
    # According to pytorch.org/docs/stable/notes/randomness.html, these have to be set
    # to get results with higher reproducibility
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False

n_devices = params["gpus"] if params["gpus"] < torch.cuda.device_count() else torch.cuda.device_count()
if n_devices > 1: # little hack to ensure that the correct gpu on kevin kauth's computer is used
    if "2080" in torch.cuda.get_device_name(torch.device("cuda:1")):
        device_str = "cuda:1"
    else:
        device_str = "cuda:0"
else:
    device_str = "cuda:0"
device = torch.device(device_str if cuda else "cpu")
if device.type == "cuda":
    print("Using %d GPU%s" %(n_devices, "s" if n_devices!=1 else ""))
else:
    print("Using CPU")

## Setup seeds
print("Set seed to", params["seed"])
set_seeds(params["seed"])

## Load raw data
print("Loading dataset %s ..." %(params["dataset"]))
trainset, valset, testset = load_raw_data(params["dataset"], DATA_PATH)

## Setup dataloader for inference and training
trainloader = torch.utils.data.DataLoader(trainset, batch_size=params["batch_size"], shuffle=True, num_workers=0, worker_init_fn=(lambda: worker_init_fn(params["seed"])))
valloader = torch.utils.data.DataLoader(valset, batch_size=params["test_batch_size"], shuffle=False, num_workers=0, worker_init_fn=(lambda: worker_init_fn(params["seed"])))
testloader = torch.utils.data.DataLoader(testset, batch_size=params["test_batch_size"], shuffle=False, num_workers=0, worker_init_fn=(lambda: worker_init_fn(params["seed"])))

## Setup Networks
print("Setup Networks...")
# Baseline Lenet
params_arch0 = torch.load(PRETRAIN_PATH+'/LENETM/params.pt')
net_arch0 = LeNet_M(not params_arch0['no_batchnorm'], not params_arch0['no_biases']).to(device)
results_arch0 = torch.load(PRETRAIN_PATH+'/LENETM/results.pt', map_location=device)
net_arch0.load_state_dict(results_arch0['model_best'])
net_arch0.eval()

# FC3
params_arch1 = torch.load(PRETRAIN_PATH+'/FC3/params.pt')
net_arch1 = FC3(not params_arch1['no_biases'], 10).to(device)
results_arch1 = torch.load(PRETRAIN_PATH+'/FC3/results.pt', map_location=device)
net_arch1.load_state_dict(results_arch1['model_best'])
net_arch1.eval()

# Binary FC3
args = parser.parse_args()
args.gradient_estimator = 'bnn'
args.bias = True
args.scale = False
args.scale_type = 'mean'
args.scale_kernelwise = False
args.scale_train = False
net_arch2 = FC3_binary(args, 10).to(device)
results_arch2 = torch.load(PRETRAIN_PATH+'/FC3_binary/results.pt', map_location=device)
net_arch2.load_state_dict(results_arch2['model_best'])
net_arch2.eval()

## Setup NN for parallelization
if n_devices > 1:
    net_arch0 = nn.DataParallel(net_arch0)
### Experiment Setup (end) ###

### Do 3-stage cascaded classifier inference ###
print("Evaluate Threshold for Pass On Rate in hierarchical classifier...")

## sweep final activation type and threshold type
#for softmax in [True,False]:
for softmax in [True]:
    #for th_type in ['abs', 'diff', 'variance', 'entropy', 'kl_div', 'kurtosis', 'gbvsb']:
    for th_type in ['diff']:
        por_plot = []
        #for th2 in np.arange(0, 1.1, 0.1):
        for th2 in np.arange(0, 1.01, 0.01):
            #for th1 in np.arange(0, 1.1, 0.1):
            for th1 in np.arange(0, 1.01, 0.01):
                
                lab_arch2 = []
                out_arch2 = []
                pred_arch2 = []
                lab_arch1 = []
                out_arch1 = []
                pred_arch1 = []
                lab_arch0 = []
                out_arch0 = []
                pred_arch0 = []
                
                deb = []
                pass_amount1 = 0
                pass_amount2 = 0
                
                m = nn.Softmax(dim=-1)
                
                # do batchwise inference
                with torch.no_grad():
                    for images, labels in testloader:
                        images = images.to(device)
                        labels = labels.to(device)

                        # do inference of batch for first stage classifier
                        outputs_arch2 = net_arch2(images)
                        if softmax:
                            outputs_arch2 = m(outputs_arch2)
                        else:
                            min_vec, _ = torch.min(outputs_arch2,1)
                            pos_mat = outputs_arch2 - min_vec[:, None]
                            pos_sum_vec = torch.sum(pos_mat,1)
                            outputs_arch2 = pos_mat / pos_sum_vec[:,None]

                        # calculate subdomain for inference of next stage based on threshold
                        mask2 = get_threshold_mask(outputs_arch2, th_type, th2, labels).to(device)
                        
                        # split image batch into subsets based on threshold
                        images_arch1 = torch.index_select(images,0,mask2.nonzero().view(-1))
                        labels_arch1 = torch.index_select(labels,0,mask2.nonzero().view(-1))
                        
                        images_arch2 = torch.index_select(images,0,(1-mask2).nonzero().view(-1))
                        labels_arch2 = torch.index_select(labels,0,(1-mask2).nonzero().view(-1))
                        outputs_arch2 = torch.index_select(outputs_arch2,0,(1-mask2).nonzero().view(-1))
                        prob_arch2, predict_arch2 = torch.topk(outputs_arch2,1,dim=1) # calc prediction from output
                        


                        # count no. of samples passed on
                        pass_amount2 += images_arch1.size(0)
                        
                        # do inference of batch for second stage classifier
                        if (sum(mask2) > 0): 
                            outputs_arch1 = net_arch1(images_arch1) # Forward pass
                            if softmax:
                                outputs_arch1 = m(outputs_arch1)
                            else:
                                min_vec, _ = torch.min(outputs_arch1,1)
                                pos_mat = outputs_arch1 - min_vec[:, None]
                                pos_sum_vec = torch.sum(pos_mat,1)
                                outputs_arch1 = pos_mat / pos_sum_vec[:,None]
                                
                            # calculate subdomain for inference of next stage based on threshold
                            mask1 = get_threshold_mask(outputs_arch1, th_type, th1, labels_arch1).to(device)
                            # print(labels_arch1)
                            # print(outputs_arch1)
                            # print(mask1)
                            
                            # split image batch into subsets based on threshold
                            images_arch0 = torch.index_select(images_arch1,0,mask1.nonzero().view(-1))
                            labels_arch0 = torch.index_select(labels_arch1,0,mask1.nonzero().view(-1))
                            
                            images_arch1 = torch.index_select(images_arch1,0,(1-mask1).nonzero().view(-1))
                            labels_arch1 = torch.index_select(labels_arch1,0,(1-mask1).nonzero().view(-1))
                            outputs_arch1 = torch.index_select(outputs_arch1,0,(1-mask1).nonzero().view(-1))
                            prob_arch1, predict_arch1 = torch.topk(outputs_arch1,1,dim=1) # calc prediction from output


                            # count no. of samples passed on
                            pass_amount1 += images_arch0.size(0)
                            
                            # do inference of batch for third stage classifier
                            if (sum(mask1) > 0): 
                                outputs_arch0 = net_arch0(images_arch0) # Forward pass
                                if softmax:
                                    outputs_arch0 = m(outputs_arch0)
                                else:
                                    min_vec, _ = torch.min(outputs_arch0,1)
                                    pos_mat = outputs_arch0 - min_vec[:, None]
                                    pos_sum_vec = torch.sum(pos_mat,1)
                                    outputs_arch0 = pos_mat / pos_sum_vec[:,None]
                                    
                                prob_arch0, predict_arch0 = torch.topk(outputs_arch0,1,dim=1)

                            else:
                                labels_arch0 = torch.tensor([], dtype=torch.long)
                                outputs_arch0 = torch.tensor([], dtype=torch.float)
                                predict_arch0 = torch.tensor([], dtype=torch.long)

                        else:
                            labels_arch1 = torch.tensor([], dtype=torch.long)
                            outputs_arch1 = torch.tensor([], dtype=torch.float)
                            predict_arch1 = torch.tensor([], dtype=torch.long)
                            labels_arch0 = torch.tensor([], dtype=torch.long)
                            outputs_arch0 = torch.tensor([], dtype=torch.float)
                            predict_arch0 = torch.tensor([], dtype=torch.long)

                        # sort processed batch samples
                        lab_arch2.append(labels_arch2.cpu())
                        out_arch2.append(outputs_arch2.cpu())
                        pred_arch2.append(predict_arch2.cpu())
                        lab_arch1.append(labels_arch1.cpu())
                        out_arch1.append(outputs_arch1.cpu())
                        pred_arch1.append(predict_arch1.cpu())
                        lab_arch0.append(labels_arch0.cpu())
                        out_arch0.append(outputs_arch0.cpu())
                        pred_arch0.append(predict_arch0.cpu())

                # concat minibatch outputs
                lab_arch2 = torch.cat(lab_arch2, dim=0)
                out_arch2 = torch.cat(out_arch2, dim=0)
                pred_arch2 = torch.cat(pred_arch2, dim=0).view(-1)
                lab_arch1 = torch.cat(lab_arch1, dim=0)
                out_arch1 = torch.cat(out_arch1, dim=0)
                pred_arch1 = torch.cat(pred_arch1, dim=0).view(-1)
                lab_arch0 = torch.cat(lab_arch0, dim=0)
                out_arch0 = torch.cat(out_arch0, dim=0)
                pred_arch0 = torch.cat(pred_arch0, dim=0).view(-1)

                # calc KPI
                total_label_size = (lab_arch2.size(0)+lab_arch1.size(0)+lab_arch0.size(0))
                tp2 = (pred_arch2==lab_arch2).sum().double().item()
                tp1 = (pred_arch1==lab_arch1).sum().double().item()
                tp0 = (pred_arch0==lab_arch0).sum().double().item()
                por2 = pass_amount2/total_label_size
                if (lab_arch1.size(0)+lab_arch0.size(0)) == 0:
                    por1 = float('nan')
                else:
                    por1 = pass_amount1/(lab_arch1.size(0)+lab_arch0.size(0))
                acc = 100*(tp2+tp1+tp0)/total_label_size

                if out_arch2.nelement() == 0:
                    acc_arch2 = float('nan')
                else:
                    acc_arch2 = 100*tp2/lab_arch2.size(0)
                
                if out_arch1.nelement() == 0:
                    acc_arch1 = float('nan')
                else:
                    acc_arch1 = 100*tp1/lab_arch1.size(0)
                
                if out_arch0.nelement() == 0:
                    acc_arch0 = float('nan')
                else:
                    acc_arch0 = 100*tp0/lab_arch0.size(0)
                
                # print log to stdout
                #print('th=' + str(th) + ',por2=' + str(por2) + ',acc_whole=' + str(acc))
                print('th2=' + str(th2) + ',th1=' + str(th1) + ',por2=' + str(por2) + ',por1=' + str(por1) + ',acc_whole=' + str(acc) + ',acc_2=' + str(acc_arch2) + ',acc_1=' + str(acc_arch1) + ',acc_0=' + str(acc_arch0))
                
                # save results(initial array element hack)
                if (len(por_plot) == 0):
                    por_plot = np.array([[th2, th1, por2, por1, acc, acc_arch2, acc_arch1, acc_arch0, lab_arch2.size(0), lab_arch1.size(0), lab_arch0.size(0)]])
                else:
                    por_plot = np.append(por_plot,np.array([[th2, th1, por2, por1, acc, acc_arch2, acc_arch1, acc_arch0, lab_arch2.size(0), lab_arch1.size(0), lab_arch0.size(0)]]),axis=0)

        #
        if softmax:
            np.savetxt('3stage_softmax_' + th_type + '.csv', por_plot, delimiter=";")
        else:
            np.savetxt('3stage_lin_' + th_type + '.csv', por_plot, delimiter=";")
