# Cascaded Classifier Pareto Analysis

This repository contains a set of scripts used for the results published in *Cascaded Classifier for Pareto-Optimal Accuracy-Cost Trade-Off Using Off-The-Shelf ANNs* (**add doi here**). 

The inference is performed using pretrained models, which are included in this repository for the MNIST experiments. Due to their size, the CIFAR models are omitted and can be provided on request. Since those models do not track state-of-the-art models, we will refer to div. model collections e.g. from [pytorch](https://pytorch.org/vision/stable/models.html) or [tensorflow](https://github.com/tensorflow/models) for highest performing network performance.

The experiment sweeps for the 2-staged and 3-staged classifier of the MNIST dataset are provided in the root directory. They can be modified towards other datasets and pretrained models. The result of the sweeps are stored in csv-files, which can be post-processed for plotting (e.g. pyplot or matlab/octave).
