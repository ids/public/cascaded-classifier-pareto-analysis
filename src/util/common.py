import numpy as np
import random
import scipy.stats as sts
#from source.networks import *
import torch
import torchvision.datasets as datasets
from torchvision.transforms import *

# Collection of utility functions

def get_threshold_mask(outputs_arch, th_type, th, labels):
    if th_type == 'abs':
        prob, pred = torch.max(outputs_arch, 1)
        mask = prob < th
    elif th_type == 'diff':
        prob, pred = torch.topk(outputs_arch,2,dim=1)
        mask = prob[:,0]-prob[:,1] < th
    elif th_type == 'variance':
        vari = np.var(outputs_arch.cpu().numpy(),axis=1)
        mask = np.var(outputs_arch.cpu().numpy(),axis=1) / 0.1 < th
        mask = torch.from_numpy(mask.astype(int))
    elif th_type == 'entropy':
        num_row = 0
        entr = np.empty(outputs_arch.cpu().numpy().shape[0])
        for row in outputs_arch.cpu().numpy():
            entr[num_row] = sts.entropy(row)
            num_row = num_row + 1
        mask = entr / sts.entropy(np.array([0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1])) < th
        mask = torch.from_numpy(mask.astype(int))
    elif th_type == 'kl_div':
        num_row = 0
        diverg = np.empty(outputs_arch.cpu().numpy().shape[0])
        for row in outputs_arch.cpu().numpy():
            diverg[num_row] = sts.entropy((np.arange(10) == labels[num_row].cpu().numpy()).astype(np.float32),row)
            num_row = num_row + 1
        mask = diverg / sts.entropy(np.array([0,0,0,0,0,0,0,0,0,1]),np.array([0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1])) / 5 < th
        mask = torch.from_numpy(mask.astype(int))
    elif th_type == 'kurtosis':
        num_row = 0
        kurt = sts.kurtosis(outputs_arch.cpu().numpy(),axis=1)
        min = sts.kurtosis(np.array([0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1])) # flachgipflig
        max = sts.kurtosis(np.array([0,0,0,0,0,0,0,0,0,1]))                     # steilgipflig
        mask = (sts.kurtosis(outputs_arch.cpu().numpy(),axis=1)-min)/(max-min)  < th
        mask = torch.from_numpy(mask.astype(int))
    elif th_type == 'gbvsb':
        N = 10  # number of classes
        M = 5   # number of top-N classes for gbvsb metric
        prob, pred = torch.topk(outputs_arch.cpu().numpy(),10,dim=1)
        high_prob = prob[:,0:M+1]
        low_prob = prob[:,M+1:N]
        if high_prob.size(1) > 1:
            high_prob = torch.sum(high_prob,1)
        else:
            high_prob = torch.squeeze(high_prob)
        if low_prob.size(1) > 1:
            low_prob = torch.sum(low_prob,1)
        else:
            low_prob = torch.squeeze(low_prob)
        mask = (high_prob - low_prob + 1) / 2 < th
    else:
        raise AssertionError()
        
    return mask.long()

def get_accuracy(device, net, loader):
    correct = 0
    total = 0
    with torch.no_grad():
        for images, labels in loader:
            images = images.to(device)
            labels = labels.to(device)

            outputs = net(images) # Forward pass
            _, predicted = torch.max(outputs, 1) # Get prediction
            total += labels.size(0)
            correct += (predicted==labels).double().sum().item()

    return 100*correct/total

def evaluate_best(path, results_file="results.pt", data_file="params.pt", param='model_best'):
    results = torch.load(path+"/"+results_file)
    data = torch.load(path+"/"+data_file)
    set_seeds(data['seed'])

    # Setup processor
    if torch.cuda.is_available():
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False
        device = torch.device("cuda:0")
    else:
        device = torch.device("cpu")

    # Setup data
    MNIST_PATH = "data"
    mnist_train = datasets.MNIST(root=MNIST_PATH+"/", train=True, download=False, transform=transforms.Compose([transforms.ToTensor()]))
    mnist_test = datasets.MNIST(root=MNIST_PATH+"/", train=False, download=False, transform=transforms.Compose([transforms.ToTensor()]))

    valset = torch.utils.data.Subset(mnist_train, range(50000,60000))
    valloader = torch.utils.data.DataLoader(valset, batch_size=1000, shuffle=False, num_workers=0, worker_init_fn=(lambda: worker_init_fn(seed)))
    testloader = torch.utils.data.DataLoader(mnist_test, batch_size=1000, shuffle=False, num_workers=0, worker_init_fn=(lambda: worker_init_fn(seed)))

    # Setup CNN
    net = LeNet_M()
    net.load_state_dict(results[param])
    net.to(device)

    # Evaluate
    print("Validation accuracy: %.2f " %(get_accuracy(device, net, valloader)))
    print("Test accuracy: %.2f " %(get_accuracy(device, net, testloader)))

def get_dataset_statistics(dataset, path="data/"):
    transform = transforms.Compose([
        transforms.ToTensor()
    ])

    if dataset == "mnist":
        set = datasets.MNIST(root=path, train=True, download=False, transform=transform)
        set_train = torch.utils.data.Subset(set, range(50000))
        dataloader = torch.utils.data.DataLoader(set_train, batch_size=50000, shuffle=False)
    elif dataset == "cifar10":
        set = datasets.CIFAR10(root=path, train=True, download=False, transform=transform)
        set_train = torch.utils.data.Subset(set, range(45000))
        dataloader = torch.utils.data.DataLoader(set_train, batch_size=45000, shuffle=False)
    elif dataset == "svhn":
        set1 = datasets.SVHN(root=path, split="train", download=False, transform=transform)
        set2 = datasets.SVHN(root=path, split="extra", download=False, transform=transform)
        set = torch.utils.data.ConcatDataset((set1, set2))
        set_train = torch.utils.data.Subset(set, range(7325,604388))
        dataloader = torch.utils.data.DataLoader(set_train, batch_size=4096, shuffle=False)
    else:
        raise AttributeError("Dataset " + dataset + " unknown. Use mnist or cifar10.")

    mean_count = 0
    std_count = 0
    batches = 0
    for im,_ in dataloader:
        im = im.view(im.shape[0], im.shape[1], -1)
        mean_count += im.mean(2).sum(0)/im.shape[0]
        std_count += im.std(2).sum(0)/im.shape[0]
        batches += 1

    print("Mean: ", end=" ")
    print(mean_count/batches)

    print("Std: ", end=" ")
    print(std_count/batches)

def otsu(img):
    img = 256*img
    mean_weight = 1.0/(img.shape[0] * img.shape[1])
    his, bins = np.histogram(img, np.array(range(0, 256)))

    final_thresh = -1
    final_value = -1
    for t in bins[1:-1]:
        w0 = np.sum(his[:t]) * mean_weight
        w1 = np.sum(his[t:]) * mean_weight

        mu0 = np.mean(his[:t])
        mu1 = np.mean(his[t:])

        value = w0 * w1 * (mu0 - mu1) ** 2

        if value > final_value:
            final_thresh = t
            final_value = value

    binary_img = img.clone()
    binary_img[img > final_thresh] = 1
    binary_img[img < final_thresh] = -1
    return binary_img, torch.FloatTensor(final_thresh)


def gen_seed(seed, delta, offset=100000):
    """Generate a (kind-of) unique seed given a base seed and a delta (currently not used anymore, probably not needed)
    """
    return seed*offset + delta

def set_seeds(seed):
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    np.random.seed(seed)
    random.seed(seed)
def worker_init_fn(seed):
    np.random.seed(seed)

def main():
    get_dataset_statistics("svhn", path="data/SVHN/")
#     transform = transforms.Compose([
#         transforms.ToTensor()
#     ])
#
#     set = datasets.MNIST(root="../data/", train=True, download=False, transform=transform)
#     tr = torch.utils.data.Subset(set, range(50000))
#     dataloader = torch.utils.data.DataLoader(tr, batch_size=50000, shuffle=False)
#     im, _ = iter(dataloader).next()
#
#     binary_mnist1 = im.clone()
#     binary_mnist1[im<0.5] = 0
#     binary_mnist1[im>=0.5] = 1
#     torch.save({"data": binary_mnist1}, "binary_mnist1.pt")
#
#     binary_mnist2 = im.clone()
#     binary_mnist2[im>0] = 1
#     torch.save({"data": binary_mnist2}, "binary_mnist2.pt")
#
#     # t = torch.zeros(50000)
#     # for i, img in enumerate(im):
#     #     if i == 1000:
#     #         print("1000")
#     #     _, t[i] = otsu(img.squeeze())
#     # # im = im.view(-1)
#     # import pdb; pdb.set_trace()
#
if __name__== "__main__":
    main()
