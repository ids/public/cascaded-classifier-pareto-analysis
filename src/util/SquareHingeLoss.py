import torch
import torch.nn as nn

class SquareHingeLoss(nn.Module):
    def __init__(self, use_cuda=True, num_outputs=10):
        super(SquareHingeLoss, self).__init__()
        self.use_cuda = use_cuda
        self.num_outputs = num_outputs

    def forward(self, input, target):
        target = torch.eye(self.num_outputs)[target] # one-hot the target vector
        target = 2*target - 1 # convert (0,1) to (-1,1)
        if(self.use_cuda and torch.cuda.is_available()): target = target.cuda()

        output = 1-input*target
        output[output.le(0)] = 0 # equals: max(0, output)
        return torch.mean(torch.sum(output*output, dim=1))
