#####
# Utility functions to load datasets
#####
### TODO
# Strip imports

### Imports
## 3rd party lib
import torch
from torchvision.transforms import *
import torchvision.datasets as datasets

def load_raw_data(dataset_str, data_path):
    if dataset_str == "mnist":
        try:
            # TODO: Checkout (from BNN.pytorch) transform=transforms.Compose([transforms.ToTensor(),transforms.Normalize((0.1307,), (0.3081,))])
            # transf = [transforms.ToTensor(), transforms.Normalize((0.5,), (0.5,))] if args.normalize_binary else [transforms.ToTensor()]
            transf = [transforms.ToTensor()]

            train_all = datasets.MNIST(root=data_path+"/", train=True, download=False, transform=transforms.Compose(transf))
            test_all = datasets.MNIST(root=data_path+"/", train=False, download=False, transform=transforms.Compose(transf))
        except RuntimeError as err:
            print("Did you forget to download the dataset? -> data/download.py")
            raise err

        trainset = torch.utils.data.Subset(train_all, range(50000))
        valset = torch.utils.data.Subset(train_all, range(50000,60000))
        
        return trainset, valset, test_all

        ## Setup CNN
        # if args.binarize is True:
            # net = LeNet_M_binary(args).to(device)
        # else:
            # net = LeNet_M(args).to(device)
    elif dataset_str == "cifar10":
        transform_train = transforms.Compose([transforms.ToTensor(), transforms.Normalize((0.5,0.5,0.5),(0.5,0.5,0.5))])
        transform_test = transforms.Compose([transforms.ToTensor(), transforms.Normalize((0.5,0.5,0.5),(0.5,0.5,0.5))])

        try:
            train_all = datasets.CIFAR10(root=data_path+"/", train=True, download=False, transform=transform_train)
            test_all = datasets.CIFAR10(root=data_path+"/", train=False, download=False, transform=transform_test)
        except RuntimeError as err:
            print("Did you forget to download the dataset? -> data/download.py")
            raise err

        trainset = torch.utils.data.Subset(train_all, range(45000))
        valset = torch.utils.data.Subset(train_all, range(45000,50000))

        return trainset, valset, test_all
        
        ## Setup CNN
        # if args.binarize is True:
            # net = VGGNet_7_binary(args).to(device)
        # else:
            # net = VGGNet_7("cifar10").to(device)
    elif dataset_str == "svhn":
        # three datasets: train (73257), test (26032), extra (531131)
        #  -> train contains images with numbers to classify in center, and parts of other numbers on the sides (sometimes whole other numbers on the sides)
        #  -> extra contains easier images, only one number in it (can be interpreted as augmentation for train)
        #  -> validation split not clearly defined. Courbariaux et al (Binary Neural Networks) use PyLearn2 library, the library's behaviour is copied here
        #      ---> 6000 validation images. 400 of each class from train, 200 of each class from extra [https://github.com/lisa-lab/pylearn2/blob/master/pylearn2/datasets/svhn.py]

        try:
            set1 = datasets.SVHN(root=data_path+"/SVHN/", split='train', download=False, transform=transforms.ToTensor())
            set2 = datasets.SVHN(root=data_path+"/SVHN/", split='extra', download=False, transform=transforms.ToTensor())

            test_all = datasets.SVHN(root=data_path+"/SVHN/", split='test', download=False, transform=transforms.ToTensor())
        except RuntimeError as err:
            print("Did you forget to download the dataset? -> data/download.py")
            raise err

        train_spc, extra_spc = 400, 200 # samples per class taken from train/extra for validation set
        # collect indices from train and extra for validation set
        val_idcs_from_train, val_idcs_from_extra = np.zeros(10*train_spc, dtype=int), np.zeros(10*extra_spc, dtype=int)
        for i in range(10): # TODO: find less messy way to collect indices
            indices = np.where(set1.labels==i)[0]
            np.random.shuffle(indices)
            val_idcs_from_train[i*train_spc:(i+1)*train_spc] = indices[0:train_spc]

            indices = np.where(set2.labels==i)[0]
            np.random.shuffle(indices)
            val_idcs_from_extra[i*extra_spc:(i+1)*extra_spc] = indices[0:extra_spc]
        # create masked indices to retrieve left-over data for train set
        train_idcs_from_train, train_idcs_from_extra = np.ones(set1.data.shape[0], dtype=bool), np.ones(set2.data.shape[0], dtype=bool)
        train_idcs_from_train[val_idcs_from_train], train_idcs_from_extra[val_idcs_from_extra] = False, False

        train_X, train_y = torch.tensor(np.concatenate((set1.data[train_idcs_from_train], set2.data[train_idcs_from_extra]), axis=0)), torch.tensor(np.concatenate((set1.labels[train_idcs_from_train], set2.labels[train_idcs_from_extra]), axis=0))
        val_X, val_y = torch.tensor(np.concatenate((set1.data[val_idcs_from_train], set2.data[val_idcs_from_extra]), axis=0)), torch.tensor(np.concatenate((set1.labels[val_idcs_from_train], set2.labels[val_idcs_from_extra]), axis=0))

        trainset = TensorTransformDataset(tensors=(train_X, train_y), transform=transforms.Compose([transforms.ToTensor(), transforms.Normalize((0.5,0.5,0.5),(0.5,0.5,0.5))]))
        valset = TensorTransformDataset(tensors=(val_X, val_y), transform=transforms.Compose([transforms.ToTensor(), transforms.Normalize((0.5,0.5,0.5),(0.5,0.5,0.5))]))

        del val_X, val_y, train_X, train_y # clean up
        gc.collect() # clean up supreme
        
        return trainset, valset, test_all

        ## Setup CNN
        # if args.binarize is True:
            # net = VGGNet_7_binary(args).to(device)
        # else:
            # net = VGGNet_7("svhn").to(device)
    else:
        raise AttributeError("Dataset " + dataset_str + " unknown. Use mnist, cifar10 or svhn.")