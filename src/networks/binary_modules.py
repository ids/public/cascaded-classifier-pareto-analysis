import torch
import torch.nn as nn

class BinaryLinear(nn.Linear):
    def __init__(self, *kargs, **kwargs):
        if "stoch_bin" in kwargs:
            self.stoch_bin = kwargs["stoch_bin"]
            kwargs.pop("stoch_bin", None)
        else:
            self.stoch_bin = False

        args = kwargs["args"]
        kwargs.pop("args", None)
        self.scale = args.scale
        self.scale_type = args.scale_type
        self.scale_kernelwise = args.scale_kernelwise
        self.scale_train = args.scale_train

        super(BinaryLinear, self).__init__(*kargs, **kwargs)

    def forward(self, input):
        if not hasattr(self.weight,'org'):
            self.weight.org = self.weight.data.clone()

        self.weight.data = binarize(self.weight.org, self.stoch_bin)

        out = nn.functional.linear(input, self.weight)

        if not self.bias is None:
            self.bias.org = self.bias.data.clone()
            out += self.bias.view(1, -1).expand_as(out)

        if self.scale:
            # As opposed to conv-layers, a kernelwise scale factors would not
            # make sense here -> we would get one scale factor per neuron in
            # an fc layer, making the layers not binary anymore
            if self.scale_type == "mean":
                alpha = self.weight.org.data.abs().mean()
            elif self.scale_type == "median":
                alpha = self.weight.org.data.abs().median()
            elif self.scale_type == "l1":
                N = self.weight.numel()
                alpha = self.weight.org.data.abs().sum()/N
            else:
                raise AttributeError("Scale-type " + self.scale_type + " unknown or not implemented yet. Use mean or median.")

            out *= alpha

        return out

class BinaryConv2d(nn.Conv2d):
    def __init__(self, *kargs, **kwargs):
        if "stoch_bin" in kwargs:
            self.stoch_bin = kwargs["stoch_bin"]
            kwargs.pop("stoch_bin", None)
        else:
            self.stoch_bin = False

        args = kwargs["args"]
        kwargs.pop("args", None)
        self.scale = args.scale
        self.scale_type = args.scale_type
        self.scale_kernelwise = args.scale_kernelwise
        self.scale_train = args.scale_train

        super(BinaryConv2d, self).__init__(*kargs, **kwargs)

    def forward(self, input):
        if not hasattr(self.weight,'org'):
            self.weight.org = self.weight.data.clone()

        self.weight.data = binarize(self.weight.org, self.stoch_bin)

        out = nn.functional.conv2d(input, self.weight, None, self.stride,
                                   self.padding, self.dilation, self.groups)

        if not self.bias is None:
            self.bias.org = self.bias.data.clone()
            out += self.bias.view(1, -1, 1, 1).expand_as(out)

        if self.scale:
            if self.scale_type == "mean":
                if self.scale_kernelwise:
                    alpha = self.weight.org.data.abs().mean(1, keepdim=True).mean(2, keepdim=True).mean(3, keepdim=True).view(1, out.shape[1], 1, 1).expand(out.shape[0], out.shape[1], 1, 1)
                else:
                    alpha = self.weight.org.data.abs().mean()
            elif self.scale_type == "median":
                if self.scale_kernelwise:
                    alpha = self.weight.org.data.abs().median(1, keepdim=True)[0].median(2, keepdim=True)[0].median(3, keepdim=True)[0].view(1, out.shape[1], 1, 1).expand(out.shape[0], out.shape[1], 1, 1)
                else:
                    alpha = self.weight.org.data.abs().median()
            elif self.scale_type == "l1":
                if self.scale_kernelwise:
                    N = self.weight.shape[1] * self.weight.shape[2] * self.weight.shape[3]
                    alpha = (self.weight.org.data.abs().sum(1, keepdim=True).sum(2, keepdim=True).sum(3, keepdim=True)/N).view(1, out.shape[1], 1, 1).expand(out.shape[0], out.shape[1], 1, 1)
                else:
                    N = self.weight.numel()
                    alpha = self.weight.org.data.abs().sum()/N
            else:
                raise AttributeError("Scale-type " + self.scale_type + " unknown or not implemented yet. Use mean or median.")

            out *= alpha

        return out

def binarize(tensor, stoch_bin=False, thresh=0):
    if stoch_bin:
        print("Doing stoch bin")
        return tensor.add_(1).div_(2).add_(torch.rand(tensor.size()).add(-0.5)).clamp_(0,1).round().mul_(2).add_(-1)
    else:
        if thresh==0:
            tensor_o = tensor.sign()
            tensor_o[tensor_o==0] = 1
        else:
            tensor_o = tensor.clone()
            tensor_o[tensor>=thresh] = 1
            tensor_o[tensor<thresh] = -1

        return tensor_o
