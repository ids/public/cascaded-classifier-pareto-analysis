from torch import nn
from torch.nn.functional import relu, max_pool2d, tanh, avg_pool2d, hardtanh
from src.networks.binary_modules import *

class LeNet_M_binary(nn.Module):
    def __init__(self, args): # gradient_estimator, layer_order, binarize_input, batch_norm, bias):
        super(LeNet_M_binary, self).__init__()
        self.conv1 = BinaryConv2d(1, 32, (5,5), padding=2, bias=not args.no_biases)
        self.conv2 = BinaryConv2d(32, 64, (5,5), bias=not args.no_biases)
        self.fc1   = BinaryLinear(64*5*5, 512, bias=not args.no_biases)
        self.fc2   = BinaryLinear(512, 10, bias=not args.no_biases)

        self.batch_norm = not args.no_batchnorm
        if self.batch_norm:
            self.bn1 = nn.BatchNorm2d(32)
            self.bn2 = nn.BatchNorm2d(64)
            self.bn3 = nn.BatchNorm1d(512)
            self.bn4 = nn.BatchNorm1d(10, affine=False)

        self.ste = args.gradient_estimator
        if self.ste == "vanilla":
            self.ste_fct = lambda x: x # anonymous function that returns unchanged input (aka "do_nothing()")
        elif self.ste == "bnn":
            self.ste_fct = hardtanh
        else:
            raise AttributeError("Straight-through estimator " + self.ste + " unknown or not implemented yet. Use vanilla or bnn.")

        self.layer_order = args.layer_order
        if self.layer_order != "CBAP" and self.layer_order != "CPBA":
            raise AttributeError("Layer-order " + self.layer_order + " unknown or not implemented yet. Use CBAP or CPBA.")

        self.binarize_input = args.binarize_input

        self.scale = args.scale
        self.scale_type = args.scale_type
        self.scale_resolution = args.scale_resolution

        nn.init.xavier_normal_(self.conv1.weight)
        nn.init.xavier_normal_(self.conv2.weight)
        nn.init.xavier_normal_(self.fc1.weight)
        nn.init.xavier_normal_(self.fc2.weight)

    def forward(self, x):
        if self.binarize_input: x.data = binarize(x.data)

        if self.layer_order == "CBAP":
            x = self.conv1(x)
            if self.batch_norm: x = self.bn1(x)
            x = self.ste_fct(x)
            x.data = binarize(x.data) # Use x.data so that gradient of binarize() is not saved in differentiation graph (gradient of ste_fct() is its substitute)
            x = max_pool2d(x, (2,2))

            x = self.conv2(x)
            if self.batch_norm: x = self.bn2(x)
            x = self.ste_fct(x)
            x.data = binarize(x.data) # Use x.data so that gradient of binarize() is not saved in differentiation graph (gradient of ste_fct() is its substitute)
            x = max_pool2d(x, (2,2))
        else:
            x = self.conv1(x)
            x = max_pool2d(x, (2,2))
            if self.batch_norm: x = self.bn1(x)
            x = self.ste_fct(x)
            x.data = binarize(x.data)

            x = self.conv2(x)
            x = max_pool2d(x, (2,2))
            if self.batch_norm: x = self.bn2(x)
            x = self.ste_fct(x)
            x.data = binarize(x.data)

        x = x.view(-1, num_flat_features(x))

        x = self.fc1(x)
        if self.batch_norm: x = self.bn3(x)
        x = self.ste_fct(x)
        x.data = binarize(x.data)

        x = self.fc2(x)
        if self.batch_norm: x = self.bn4(x)
        return x

class LeNet_M(nn.Module):
    """Modified LeNet-5 CNN architecture

    The architecture is mainly inspired by the MNIST network used in
    "Ternary Weight Networks" by Li et al., which in turn resembles a LeNet-5
    architecture - thus the name.
    """
    def __init__(self, batch_norm, bias):
        super(LeNet_M, self).__init__()
        self.conv1 = nn.Conv2d(1, 32, (5,5), padding=2, bias=bias)
        self.conv2 = nn.Conv2d(32, 64, (5,5), bias=bias)
        self.fc1   = nn.Linear(64*5*5, 512, bias=bias)
        self.fc2   = nn.Linear(512, 10, bias=bias)
        if batch_norm: self.bn1 = nn.BatchNorm2d(32)
        if batch_norm: self.bn2 = nn.BatchNorm2d(64)
        if batch_norm: self.bn3 = nn.BatchNorm1d(512)
        if batch_norm: self.bn4 = nn.BatchNorm1d(10, affine=False) # TODO

        self.batch_norm = batch_norm

        nn.init.xavier_normal_(self.conv1.weight)
        nn.init.xavier_normal_(self.conv2.weight)
        nn.init.xavier_normal_(self.fc1.weight)
        nn.init.xavier_normal_(self.fc2.weight)

    def forward(self, x):
        x = self.conv1(x)
        if self.batch_norm: x = self.bn1(x)
        x = relu(x)
        x = max_pool2d(x, (2,2))
        x = self.conv2(x)
        if self.batch_norm: x = self.bn2(x)
        x = relu(x)
        x = max_pool2d(x, (2,2))

        x = x.view(-1, num_flat_features(x))

        x = self.fc1(x)
        if self.batch_norm: x = self.bn3(x)
        x = relu(x)
        x = self.fc2(x)
        if self.batch_norm: x = self.bn4(x)
        return x
		
class FC3(nn.Module):
    """3-layer MLP

    The architecture is mainly inspired by the minimal network depth to achieve
    an arbitrary function regression. The network architecture is modified towards
    the MNIST dataset
    """
    def __init__(self, bias, num_output):
        super(FC3, self).__init__()
        self.fc1   = nn.Linear(28*28, 512, bias=bias)
        self.fc2   = nn.Linear(512, num_output, bias=bias)

        nn.init.xavier_normal_(self.fc1.weight)
        nn.init.xavier_normal_(self.fc2.weight)

    def forward(self, x):
        x = x.view(-1, 28*28)
        x = self.fc1(x)
        x = relu(x)
        x = self.fc2(x)
        return x
        
class FC3_binary(nn.Module):
    """3-layer MLP

    The architecture is mainly inspired by the minimal network depth to achieve
    an arbitrary function regression. This is the binarized version
    """
    def __init__(self, args, num_output):
        super(FC3_binary, self).__init__()
        self.fc1   = BinaryLinear(28*28, 512, bias=args.bias, args=args)
        self.fc2   = BinaryLinear(512, num_output, bias=args.bias, args=args)
        
        self.bn1 = nn.BatchNorm1d(512)
        self.bn2 = nn.BatchNorm1d(num_output, affine=False)

        nn.init.xavier_normal_(self.fc1.weight)
        nn.init.xavier_normal_(self.fc2.weight)
        
        self.ste = args.gradient_estimator
        if self.ste == "vanilla":
            self.ste_fct = lambda x: x # anonymous function that returns unchanged input (aka "do_nothing()")
        elif self.ste == "bnn":
            self.ste_fct = hardtanh
        else:
            raise AttributeError("Straight-through estimator " + self.ste + " unknown or not implemented yet. Use vanilla or bnn.")

    def forward(self, x):
        x = x.view(-1, 28*28)
        x = self.fc1(x)
        #if self.batch_norm: x = self.bn1(x)
        x = self.bn1(x)
        x = self.ste_fct(x)
        x.data = binarize(x.data)
        x = self.fc2(x)
        #if self.batch_norm: x = self.bn2(x)
        x = self.bn2(x)
        return x

class LeNet(nn.Module):
    """LeNet CNN architecture

    The architecture is the closest fit to the IDS' MATLAB implementation of
    LeNet-5, introduced by LeCun et al. in "Gradient-based learning applied to
    document recognition". Missing features: weighted pool layers, special layer
    connection between the first pool and the second conv layer.

    For easy experiments, the pooling and activation types are given as args
    during instantiation, as well as whether to apply an activation to the
    outputs of pooling layers.

    Example: net = LeNet("max", "stanh", True)
    """
    def __init__(self, poolType, actType, usePoolAct):
        super(LeNet, self).__init__()
        self.conv1 = nn.Conv2d(1, 6, (5,5), padding=2)
        self.conv2 = nn.Conv2d(6, 16, (5,5))
        self.fc1   = nn.Linear(16*5*5, 120)
        self.fc2   = nn.Linear(120, 84)
        self.fc3   = nn.Linear(84, 10)

        if actType == 'relu':
            self.act = relu
        elif actType == 'stanh':
            self.act = self.stanh

        if poolType == 'sum':
            self.pool = self.sum_pool2d
        elif poolType == 'avg':
            self.pool = avg_pool2d
        elif poolType == 'max':
            self.pool = max_pool2d

        self.usePoolAct = usePoolAct

        nn.init.xavier_normal_(self.conv1.weight)
        nn.init.xavier_normal_(self.conv2.weight)
        nn.init.xavier_normal_(self.fc1.weight)
        nn.init.xavier_normal_(self.fc2.weight)
        nn.init.xavier_normal_(self.fc3.weight)

    def forward(self, x):
        x = self.pool(self.act(self.conv1(x)), (2,2))
        if self.usePoolAct: x = self.act(x)
        x = self.pool(self.act(self.conv2(x)), (2,2))
        if self.usePoolAct: x = self.act(x)
        x = x.view(-1, num_flat_features(x))
        x = self.act(self.fc1(x))
        x = self.act(self.fc2(x))
        x = self.fc3(x)
        return x

    def stanh(self, x):
        return 1.7159 * tanh((2/3) * x)

    def sum_pool2d(self, x, poolSize):
        x = 4*avg_pool2d(x, poolSize)

def num_flat_features(x):
    size = x.size()[1:]
    num_features = 1
    for s in size:
        num_features *= s

    return num_features
