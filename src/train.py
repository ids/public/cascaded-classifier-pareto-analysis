import torch
import torchvision
from torchvision.transforms import *

import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F
import torch.utils as utils

from copy import deepcopy

import numpy as np
import time

from src.util.SquareHingeLoss import *
from src.util.common import get_accuracy, set_seeds, gen_seed
from src.networks import *

from operator import itemgetter

def train(device, net, trainloader, valloader, testloader, params, results_path, save_models=False):
    net.train()

    t_start = time.time()
    results = {'losses': [], 'accuracies': [], 'model_initial': net.state_dict()}

    max_val_accuracy = 0

    criterion = SquareHingeLoss(use_cuda=(not params["no_cuda"]), num_outputs=10) if params["loss"] == "SquareHingeLoss" else nn.CrossEntropyLoss()

    l2_weight_decay = params["weight_decay"] if params["regularization"] == 'L2' else 0

    if params["optimizer"] == "sgd":
        optimizer = optim.SGD(net.parameters(), lr=params["learning_rate"], momentum=params["momentum"], weight_decay=l2_weight_decay, nesterov=params["nesterov"])
    else:
        raise AttributeError("Optimizer " + self["optimizer"] + " unknown or not usable yet. Use sgd or adam.")
        
    # if params.resume:
        # optimizer.load_state_dict(torch.load(params.model)['optimizer'])


    #if params.resume:
    #    scheduler.load_state_dict(torch.load(params.model)['scheduler'])

    print(params)

    ms_idx, decay_idx = 0, 0
    print("Starting training...")
    running_loss_epochs = len(trainloader) / params["loss_resolution"] # output
    try:
        for epoch in range(params["epochs"]): # for all epochs
            #learn rate decay
            if len(params["lr_milestones"])>0 and epoch==params["lr_milestones"][ms_idx]:
                print('milestone: %d' %(epoch))
                print('decay: %f' %(params["lr_decay"][decay_idx]))

                decay = params["lr_decay"][decay_idx]
                for g in optimizer.param_groups:
                    g['lr'] = decay * g['lr']
                if(len(params["lr_milestones"]) > ms_idx+1): ms_idx = ms_idx + 1
                if(len(params["lr_decay"]) > decay_idx+1): decay_idx = decay_idx + 1

            running_loss = 0.0
            t_start_epoch = time.time()
            for i, (images, labels) in enumerate(trainloader): # for all minibatches
                images, labels = images.to(device), labels.to(device)

                optimizer.zero_grad()

                outputs = net(images) # Forward pass
                loss = criterion(outputs, labels) # Loss computation
                if params["regularization"] != 'L2':
                    loss = loss + params["weight_decay"] * reg_term(net, params["regularization"])
                loss.backward() # Backward pass

                if params["binarize"]:
                    for p in list(net.parameters()): # Copy full-precision weights into main params
                        if hasattr(p, 'org'):
                            p.data.copy_(p.org)

                optimizer.step()
                
                if params["binarize"]:
                    for p in list(net.parameters()):
                        if hasattr(p,'org'):
                            # if args.weight_clipping:
                                # p.data.clamp_(-1,1)
                            p.org.copy_(p.data) # Clip weights and save them in .org

                running_loss += loss.item()
                if i%running_loss_epochs == running_loss_epochs-1:
                    print('[%d, %d (%.2f%%)] loss: %.5f' % (epoch, i+1, 100*(epoch*len(trainloader)+i+1)/(params["epochs"]*len(trainloader)),running_loss / running_loss_epochs))
                    results['losses'].append(running_loss)
                    running_loss = 0.0

            net.eval()
            accuracy = get_accuracy(device, net, valloader)
            #accuracy_train = get_accuracy(device, net, trainloader)
            net.train()
            if accuracy > max_val_accuracy:
                max_val_accuracy = accuracy
                max_val_accuracy_model = deepcopy(net.state_dict())
                max_val_accuracy_optimizer = deepcopy(optimizer.state_dict())
                #max_val_accuracy_scheduler = deepcopy(scheduler.state_dict())
                max_val_accuracy_epoch = epoch
                max_string = " (current maximum)"
            else:
                max_string = ""

            results['accuracies'].append(accuracy)

            #print("Accuracy (train set) : %.2f %%" %(accuracy_train))
            print("Accuracy (validation set) %s: %.2f %%" %(max_string, accuracy))
            print("Time for epoch: %.3f s" %(time.time() - t_start_epoch))

            if save_models:
                torch.save(net.state_dict(), results_path+"/model_epoch"+str(epoch)+".pt")
    except KeyboardInterrupt as err:
        torch.save({'epoch': epoch, 'model': net.state_dict(), 'opt': optimizer.state_dict(), 'error': str(err)}, results_path + "/error_checkpoint.pt")
        print(err)

    results['val_accuracy_best'] = max_val_accuracy
    results['epoch_best'] = max_val_accuracy_epoch
    results['model_best'] = max_val_accuracy_model
    results['optimizer_best'] = max_val_accuracy_optimizer
    #results['scheduler_best'] = max_val_accuracy_scheduler
    results['model_final'] = net.state_dict()
    results['optimizer_final'] = optimizer.state_dict()
    #results['scheduler_final'] = scheduler.state_dict()

    print('Finished Training in %.3f s' %(time.time() - t_start))
    print('Evaluating best model on testset...')

    # if params["dataset"] == "mnist":
        # # Need to create new net from scratch so that full-precision weights that are saved as backup are deleted fully
        # net = LeNet_M_binary(params).to(device) if params.binarize else LeNet_M(params).to(device)

        # # In inference, the layer order should not influence the networks' outputs (mathematically proven)
        # # -> however, due to numerical instabilities some numbers very close to zero do change their sign if another layer order is applied
        # # -> in my fpga inference engine, CBAP is the chosen layer. For better comparibility, manually set the layer order for the test inference to CBAP
        # net.layer_order = "CBAP"
    # else:
        # net = VGGNet_7_binary(params).to(device) if params.binarize else VGGNet_7(params.dataset).to(device)
        # net.layer_order = "CBAP"

    net.load_state_dict(max_val_accuracy_model)  # Load model with best performance on validation set
    net.eval()
    test_accuracy = get_accuracy(device, net, testloader) # Evaluate on test set
    results['test_accuracy'] = test_accuracy

    print('##########')
    print('-> Saving results to %s/results.pt' %(results_path))
    print('-> Maximum validation accuracy: %.2f %% in epoch %d' %(max_val_accuracy, max_val_accuracy_epoch))
    print('-> Accuracy of best model on testset: %.2f %%' %(test_accuracy))
    print('##########')

    np.savetxt(results_path + "/accuracies.txt", np.array(results['accuracies'])) # for convenience
    np.savetxt(results_path + "/test_accuracy.txt", np.array([test_accuracy])) # for convenience
    torch.save(results, results_path + "/results.pt")

def reg_term(net, type):
    sum = 0
    for p in list(net.parameters()):
        if hasattr(p,'org'):
            # Horrible implementation incoming
            # -> In order for loss.backward() to track the regularization loss,
            #    we need to calculate it using the current parameters of the
            #    network (as they are tracked themselves). At this stage, they
            #    are binary - but we need to calculate the loss using the
            #    full-precision weights. Current workaround: copy ALL
            #    full-precision weights into the networks parameters (which are
            #    saved in a temp-tensor), then calculate loss, then copy the
            #    back the binary parameters. :(
            temp = torch.Tensor(p.data.shape)
            temp.copy_(p.data)
            p.data.copy_(p.org)
            sum += (1 - p**2).sum()
            p.org.copy_(p.data)
            p.data.copy_(temp)
    return sum
