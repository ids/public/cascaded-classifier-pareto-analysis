#####
# Template for executing model training / inference 
#####

### Imports
## 3rd party lib
import argparse
from datetime import datetime
import gc
import joblib
import numpy as np
import os
import random
from sklearn import cluster
import torch
import torchvision.datasets as datasets
import yaml

## Custom lib
from src.networks.networks import *
from src.util.common import set_seeds
from src.util.load_dataset import *
from src.train import train


### Constants
## Arguments
parser = argparse.ArgumentParser(description='NN Template - Experiment')
parser.add_argument('id', type=str, help='a unique id for experiment identification')
parser.add_argument('--config-path', type=str, default='config/config_default.yml',
                    help='path to config file of parameters')
ARGS = parser.parse_args()

## Paths
DATA_PATH = "data"
PRETRAIN_PATH = "pretrained_models"
RESULTS_PATH = "results/" + ARGS.id + "/" + datetime.now().strftime("%Y-%m-%d_%H%M%S")
if not os.path.exists(RESULTS_PATH): os.makedirs(RESULTS_PATH)

### Source begin
## parse YAML file for input parameter
print("Parse YAML...")
with open(ARGS.config_path, 'r') as stream:
    try:
        params = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)

## Setup CUDA (or not)
print("Setup CUDA...")
cuda = not params["no_cuda"] and torch.cuda.is_available()
if cuda:
    # According to pytorch.org/docs/stable/notes/randomness.html, these have to be set
    # to get results with higher reproducibility
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False

n_devices = params["gpus"] if params["gpus"] < torch.cuda.device_count() else torch.cuda.device_count()
if n_devices > 1: # little hack to ensure that the correct gpu on kevin kauth's computer is used
    if "2080" in torch.cuda.get_device_name(torch.device("cuda:1")):
        device_str = "cuda:1"
    else:
        device_str = "cuda:0"
else:
    device_str = "cuda:0"
device = torch.device(device_str if cuda else "cpu")
if device.type == "cuda":
    print("Using %d GPU%s" %(n_devices, "s" if n_devices!=1 else ""))
else:
    print("Using CPU")

## Setup seeds
print("Set seed to", params["seed"])
set_seeds(params["seed"])

## Load raw data
print("Loading dataset %s ..." %(params["dataset"]))
trainset, valset, testset = load_raw_data(params["dataset"], DATA_PATH)


## Setup dataloader for inference and training
trainloader = torch.utils.data.DataLoader(trainset, batch_size=params["batch_size"], shuffle=True, num_workers=0, worker_init_fn=(lambda: worker_init_fn(params["seed"])))
valloader = torch.utils.data.DataLoader(valset, batch_size=params["test_batch_size"], shuffle=False, num_workers=0, worker_init_fn=(lambda: worker_init_fn(params["seed"])))
testloader = torch.utils.data.DataLoader(testset, batch_size=params["test_batch_size"], shuffle=False, num_workers=0, worker_init_fn=(lambda: worker_init_fn(params["seed"])))

## Setup NN for parallelization
if n_devices > 1:
    net = nn.DataParallel(net)

## Setup Network
print("Setup Network...")

# args init hack
args = parser.parse_args()

args.gradient_estimator = 'bnn'
args.bias = True
args.scale = False
args.scale_type = 'l1'
args.scale_kernelwise = False
args.scale_train = False
net = FC3_binary(args, 10).to(device)


## Do stuff
print("Do stuff...")

train(device, net, trainloader, valloader, testloader, params, RESULTS_PATH)



