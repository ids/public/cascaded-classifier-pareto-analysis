import torchvision
import sys

if len(sys.argv) > 1:
    dataset = sys.argv[1]
else:
    raise RuntimeError("Missing argument: name of dataset (mnist, cifar10, svhn, all)")

if dataset == "mnist":
    torchvision.datasets.MNIST(root="./", train=True, download=True)
    torchvision.datasets.MNIST(root="./", train=False, download=True)
elif dataset == "cifar10":
    torchvision.datasets.CIFAR10(root="./", train=True, download=True)
    torchvision.datasets.CIFAR10(root="./", train=False, download=True)
elif dataset == "svhn": #NEED TO INSTALL SCIPY FOR THIS
    torchvision.datasets.SVHN(root="./", split='train', download=True)
    torchvision.datasets.SVHN(root="./", split='test', download=True)
    torchvision.datasets.SVHN(root="./", split='extra', download=True)
elif dataset == "all":
    torchvision.datasets.MNIST(root="./", train=True, download=True)
    torchvision.datasets.MNIST(root="./", train=False, download=True)
    torchvision.datasets.CIFAR10(root="./", train=True, download=True)
    torchvision.datasets.CIFAR10(root="./", train=False, download=True)
    torchvision.datasets.SVHN(root="./", split='train', download=True)
    torchvision.datasets.SVHN(root="./", split='test', download=True)
    torchvision.datasets.SVHN(root="./", split='extra', download=True)  
