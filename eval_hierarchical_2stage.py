#####
# Template for executing model training / inference 
#####

### Imports
## 3rd party lib
import argparse
from datetime import datetime
import gc
import matplotlib.pyplot as plt
import numpy as np
import os
import random
import scipy.stats as sts
import torch
import torchvision.datasets as datasets
import yaml

## Custom lib
from src.networks.networks import *
from src.util.common import set_seeds
from src.util.load_dataset import *


### Constants
## Arguments
parser = argparse.ArgumentParser(description='NN Template - Experiment')
#parser.add_argument('id', type=str, help='a unique id for experiment identification')
parser.add_argument('--config-path', action='store_true', default='config/config_hierarchical.yml',
                    help='path to config file of parameters')
ARGS = parser.parse_args()

## Paths
DATA_PATH = "data"
PRETRAIN_PATH = "pretrained_models"
#RESULTS_PATH = "results/" + ARGS.id + "/" + datetime.now().strftime("%Y-%m-%d_%H%M%S")
#if not os.path.exists(RESULTS_PATH): os.makedirs(RESULTS_PATH)

### Source begin
## parse YAML file for input parameter
print("Parse YAML...")
with open(ARGS.config_path, 'r') as stream:
    try:
        params = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)

## Setup CUDA (or not)
print("Setup CUDA...")
cuda = not params["no_cuda"] and torch.cuda.is_available()
if cuda:
    # According to pytorch.org/docs/stable/notes/randomness.html, these have to be set
    # to get results with higher reproducibility
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False

n_devices = params["gpus"] if params["gpus"] < torch.cuda.device_count() else torch.cuda.device_count()
if n_devices > 1: # little hack to ensure that the correct gpu on kevin kauth's computer is used
    if "2080" in torch.cuda.get_device_name(torch.device("cuda:1")):
        device_str = "cuda:1"
    else:
        device_str = "cuda:0"
else:
    device_str = "cuda:0"
device = torch.device(device_str if cuda else "cpu")
if device.type == "cuda":
    print("Using %d GPU%s" %(n_devices, "s" if n_devices!=1 else ""))
else:
    print("Using CPU")

## Setup seeds
print("Set seed to", params["seed"])
set_seeds(params["seed"])

## Load raw data
print("Loading dataset %s ..." %(params["dataset"]))
trainset, valset, testset = load_raw_data(params["dataset"], DATA_PATH)

## Setup dataloader for inference and training
trainloader = torch.utils.data.DataLoader(trainset, batch_size=params["batch_size"], shuffle=True, num_workers=0, worker_init_fn=(lambda: worker_init_fn(params["seed"])))
valloader = torch.utils.data.DataLoader(valset, batch_size=params["test_batch_size"], shuffle=False, num_workers=0, worker_init_fn=(lambda: worker_init_fn(params["seed"])))
testloader = torch.utils.data.DataLoader(testset, batch_size=params["test_batch_size"], shuffle=False, num_workers=0, worker_init_fn=(lambda: worker_init_fn(params["seed"])))

## Setup Networks
print("Setup Networks...")
params_small = torch.load(PRETRAIN_PATH+'/FC3/params.pt')
params_large = torch.load(PRETRAIN_PATH+'/LENETM/params.pt')
net_small = FC3(not params_small['no_biases'], 10).to(device)
net_large = LeNet_M(not params_large['no_batchnorm'], not params_large['no_biases']).to(device)
results_small = torch.load(PRETRAIN_PATH+'/FC3/results.pt', map_location=device)
results_large = torch.load(PRETRAIN_PATH+'/LENETM/results.pt', map_location=device)
net_small.load_state_dict(results_small['model_best'])
net_large.load_state_dict(results_large['model_best'])
net_small.eval()
net_large.eval()

## Setup NN for parallelization
if n_devices > 1:
    net_small = nn.DataParallel(netSmall)
    net_large = nn.DataParallel(netLarge)

## Do stuff
print("Evaluate Threshold for Pass On Rate in hierarchical classifier...")

# get last layer outputs
#for softmax in [True,False]:
for softmax in [True]:
    #for th_type in ['abs', 'diff', 'variance', 'entropy', 'kl_div', 'kurtosis', 'gbvsb']:
    for th_type in ['diff']:
        print('Start run: softmax=' + str(softmax) + ', th_type=' + str(th_type))
        por_plot = np.empty([1,3])
        for th in np.arange(0, 1.01, 0.01):
            
            out = []
            out_small = []
            out_large = []
            lab = []
            lab_small = []
            lab_large = []
            pred = []
            deb = []
            pass_amount = 0
            
            m = nn.Softmax()
            
            # do batchwise inference
            with torch.no_grad():
                for images, labels in testloader:
                    images = images.to(device)
                    labels = labels.to(device)

                    # do inference of batch for first stage classifier
                    outputs_small = net_small(images)
                    if softmax:
                        outputs_small_normed = m(outputs_small)
                    else:
                        min_vec, _ = torch.min(outputs_small,1)
                        pos_mat = outputs_small - min_vec[:, None]
                        pos_sum_vec = torch.sum(pos_mat,1)
                        outputs_small_normed = pos_mat / pos_sum_vec[:,None]

                    # calculate subdomain for inference of next stage based on threshold
                    if th_type == 'abs':
                        prob, pred = torch.max(outputs_small_normed, 1)
                        mask = prob < th
                    elif th_type == 'diff':
                        prob, pred = torch.topk(outputs_small_normed,2,dim=1)
                        mask = prob[:,0]-prob[:,1] < th
                    elif th_type == 'variance':
                        vari = np.var(outputs_small_normed.cpu().numpy(),axis=1)
                        mask = np.var(outputs_small_normed.cpu().numpy(),axis=1) / 0.1 < th
                        mask = torch.from_numpy(mask.astype(int)).to(device)
                    elif th_type == 'entropy':
                        num_row = 0
                        entr = np.empty(outputs_small_normed.cpu().numpy().shape[0])
                        for row in outputs_small_normed.cpu().numpy():
                            entr[num_row] = sts.entropy(row)
                            num_row = num_row + 1
                        mask = entr / sts.entropy(np.array([0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1])) < th
                        mask = torch.from_numpy(mask.astype(int)).to(device)
                    elif th_type == 'kl_div':
                        num_row = 0
                        diverg = np.empty(outputs_small_normed.cpu().numpy().shape[0])
                        for row in outputs_small_normed.cpu().numpy():
                            diverg[num_row] = sts.entropy((np.arange(10) == labels[num_row].cpu().numpy()).astype(np.float32),row)
                            num_row = num_row + 1
                        mask = diverg / sts.entropy(np.array([0,0,0,0,0,0,0,0,0,1]),np.array([0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1])) / 5 < th
                        mask = torch.from_numpy(mask.astype(int)).to(device)
                    elif th_type == 'kurtosis':
                        num_row = 0
                        kurt = sts.kurtosis(outputs_small_normed.cpu().numpy(),axis=1)
                        min = sts.kurtosis(np.array([0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1])) # flachgipflig
                        max = sts.kurtosis(np.array([0,0,0,0,0,0,0,0,0,1]))                     # steilgipflig
                        mask = (sts.kurtosis(outputs_small_normed.cpu().numpy(),axis=1)-min)/(max-min)  < th
                        mask = torch.from_numpy(mask.astype(int)).to(device)
                    elif th_type == 'gbvsb':
                        N = 10  # number of classes
                        M = 5   # number of top-N classes for gbvsb metric
                        prob, pred = torch.topk(outputs_small_normed.cpu().numpy(),10,dim=1)
                        high_prob = prob[:,0:M+1]
                        low_prob = prob[:,M+1:N]
                        if high_prob.size(1) > 1:
                            high_prob = torch.sum(high_prob,1)
                        else:
                            high_prob = torch.squeeze(high_prob)
                        if low_prob.size(1) > 1:
                            low_prob = torch.sum(low_prob,1)
                        else:
                            low_prob = torch.squeeze(low_prob)
                        mask = (high_prob - low_prob + 1) / 2 < th
                    else:
                        raise AssertionError()
                    
                    mask = mask.long()
                    
                    images_prev = torch.index_select(images,0,(1-mask).nonzero().view(-1))
                    labels_prev = torch.index_select(labels,0,(1-mask).nonzero().view(-1))
                    images_next = torch.index_select(images,0,mask.nonzero().view(-1))
                    labels_next = torch.index_select(labels,0,mask.nonzero().view(-1))
                    
                    # count no. of samples passed on
                    pass_amount += images_next.size(0)
                    
                    # do inference of batch for second stage classifier
                    #print(sum(mask))
                    #print(images_next.size(0))
                    if (sum(mask) > 0): 
                        outputs_large = net_large(images_next) # Forward pass
                        if softmax:
                            outputs_large_normed = m(outputs_large)
                        else:
                            min_vec_large, _ = torch.min(outputs_large,1)
                            pos_mat_large = outputs_large - min_vec_large[:, None]
                            pos_sum_vec_large = torch.sum(pos_mat_large,1)
                            outputs_large_normed = pos_mat_large / pos_sum_vec_large[:,None]

                        
                        output_final = torch.cat((torch.index_select(outputs_small_normed,0,(1-mask).nonzero().view(-1)),outputs_large_normed),0)
                        label_final = torch.cat((torch.index_select(labels,0,(1-mask).nonzero().view(-1)),labels_next),0)
                    else:
                        outputs_large_normed = torch.tensor([], dtype=torch.float)
                        labels_next = torch.tensor([], dtype=torch.long)
                        output_final = outputs_small_normed
                        label_final = labels
                    
                    # sort processed batch samples
                    out_small.append(torch.index_select(outputs_small_normed,0,(1-mask).nonzero().view(-1)).cpu())
                    out_large.append(outputs_large_normed.cpu())
                    lab_small.append(labels_prev.cpu())
                    lab_large.append(labels_next.cpu())
                    out.append(output_final.cpu())
                    lab.append(label_final.cpu())
          
            # get softmax outputs
            out = torch.cat(out, dim=0)
            lab = torch.cat(lab, dim=0)
            
            out_small = torch.cat(out_small, dim=0)
            lab_small = torch.cat(lab_small, dim=0)
            out_large = torch.cat(out_large, dim=0)
            lab_large = torch.cat(lab_large, dim=0)

            # if (th < 0.191 and th > 0.189 and th_type == 'diff' and softmax == False):
                # np.savetxt('debug_out_' + th_type + '.csv', out.cpu().numpy(), delimiter=";")
                # np.savetxt('debug_lab_' + th_type + '.csv', lab.cpu().numpy(), delimiter=";")
                # np.savetxt('debug_out_small_' + th_type + '.csv', out_small.cpu().numpy(), delimiter=";")
                # np.savetxt('debug_lab_small_' + th_type + '.csv', lab_small.cpu().numpy(), delimiter=";")
                # np.savetxt('debug_out_large_' + th_type + '.csv', out_large.cpu().numpy(), delimiter=";")
                # np.savetxt('debug_lab_large_' + th_type + '.csv', lab_large.cpu().numpy(), delimiter=";")
            # elif (th < 0.391 and th > 0.389 and th_type == 'abs' and softmax == False):
                # np.savetxt('debug_out_' + th_type + '.csv', out.cpu().numpy(), delimiter=";")
                # np.savetxt('debug_lab_' + th_type + '.csv', lab.cpu().numpy(), delimiter=";")
                # np.savetxt('debug_out_small_' + th_type + '.csv', out_small.cpu().numpy(), delimiter=";")
                # np.savetxt('debug_lab_small_' + th_type + '.csv', lab_small.cpu().numpy(), delimiter=";")
                # np.savetxt('debug_out_large_' + th_type + '.csv', out_large.cpu().numpy(), delimiter=";")
                # np.savetxt('debug_lab_large_' + th_type + '.csv', lab_large.cpu().numpy(), delimiter=";")            
            
                

            # get top 2
            prob, pred = torch.topk(out,2,dim=1)
            if out_small.nelement() != 0:
                prob_small, pred_small = torch.topk(out_small,2,dim=1)
            if out_large.nelement() != 0:
                prob_large, pred_large = torch.topk(out_large,2,dim=1)

            # calc KPI
            por = pass_amount/lab.size(0)
            acc = 100*(pred[:,0]==lab).double().sum().item()/lab.size(0)

            if out_small.nelement() == 0:
                acc_small = float('nan')
            else:
                acc_small = 100*(pred_small[:,0]==lab_small).double().sum().item()/lab_small.size(0)
            
            if out_large.nelement() == 0:
                acc_large = float('nan')
            else:
                acc_large = 100*(pred_large[:,0]==lab_large).double().sum().item()/lab_large.size(0)
            
            # print log to stdout
            print('th=' + str(th) + ',por=' + str(por) + ',acc_whole=' + str(acc) + ',acc_small=' + str(acc_small) + ',acc_large=' + str(acc_large))
            
            # save results(initial array element hack)
            if (th < 0.01):
                por_plot = np.array([[th, por, acc, acc_small, acc_large, lab_small.size(0), lab_large.size(0)]])
            else:
                por_plot = np.append(por_plot,np.array([[th, por, acc, acc_small, acc_large, lab_small.size(0), lab_large.size(0)]]),axis=0)

            # get top2 prediction sorted by tp and mc
            # tmp = (pred[:,0]==lab)
            # soft_out_tp = torch.index_select(out,0,tmp.nonzero().view(-1))
            # soft_out_mc = torch.index_select(out,0,(1-tmp).nonzero().view(-1))

            # prob_tp, pred_tp = torch.topk(soft_out_tp,2,dim=1)
            # prob_mc, pred_mc = torch.topk(soft_out_mc,2,dim=1)

            # create histogram values
            # tp
            # counts_tp,bin_edges_tp = np.histogram(prob_tp[:,0].cpu().numpy(), 10, range=[0.0, 1.0])
            # bin_centres_tp = (bin_edges_tp[:-1] + bin_edges_tp[1:])/2.

            # counts_tp_diff,bin_edges_tp_diff = np.histogram(prob_tp[:,0].cpu().numpy()-prob_tp[:,1].cpu().numpy(), 10, range=[0.0, 1.0])
            # bin_centres_tp_diff = (bin_edges_tp_diff[:-1] + bin_edges_tp_diff[1:])/2.

            # mc
            # counts_mc,bin_edges_mc = np.histogram(prob_mc[:,0].cpu().numpy(), 10, range=[0.0, 1.0])
            # bin_centres_mc = (bin_edges_mc[:-1] + bin_edges_mc[1:])/2.

            # counts_mc_diff,bin_edges_mc_diff = np.histogram(prob_mc[:,0].cpu().numpy()-prob_mc[:,1].cpu().numpy(), 10, range=[0.0, 1.0])
            # bin_centres_mc_diff = (bin_edges_mc_diff[:-1] + bin_edges_mc_diff[1:])/2.
            
        if softmax:
            np.savetxt('softmax_' + th_type + '.csv', por_plot, delimiter=";")
        else:
            np.savetxt('lin_' + th_type + '.csv', por_plot, delimiter=";")


        # fig, ax1 = plt.subplots()
        # color = 'tab:blue'
        # ax1.set_xlabel('th')
        # ax1.set_ylabel('Accuracy', color=color)
        # ax1.plot(por_plot[1:,0], por_plot[1:,2], 'b+', linestyle='-')
        # ax1.tick_params(axis='y', labelcolor=color)

        # ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

        # color = 'tab:red'
        # ax2.set_ylabel('POR', color=color)  # we already handled the x-label with ax1
        # ax2.plot(por_plot[1:,0], por_plot[1:,1], 'rx', linestyle=':')
        # ax2.tick_params(axis='y', labelcolor=color)

        # plt.xticks(np.arange(0, 1, step=0.1))
        # plt.grid(b=True, which='major', color='lightgrey', linestyle='-')
        # plt.grid(b=True, which='minor', color='lightgrey', linestyle='--')
        # plt.title('MNIST Testset, Hierarchical(FC3+Baseline), Sweep th')
        # fig.tight_layout()
        # plt.show()
